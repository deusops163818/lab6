#!/bin/zsh

set -x
set -e

docker build \
    --target openssl \
    --cache-from lab6-nginx:openssl \
    -t lab6-nginx:openssl \
    -f openssl/Dockerfile \
    openssl

docker build \
    --target system \
    --cache-from lab6-nginx:system \
    -t lab6-nginx:system \
    -f Dockerfile \
    .

docker build \
    --target build \
    --cache-from lab6-nginx:system \
    --cache-from lab6-nginx:build \
    -t lab6-nginx:build \
    -f Dockerfile \
    .

docker build \
    --target app \
    --cache-from lab6-nginx:build \
    -t lab6-nginx:app \
    -f Dockerfile \
    .
