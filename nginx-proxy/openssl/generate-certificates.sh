#!/bin/sh

while true; do
  openssl req -x509 -newkey rsa:4096 -keyout /certificates/key.pem -out /certificates/cert.pem -sha256 -days 1 -nodes -subj "/C=XX/ST=StateName/L=CityName/O=CompanyName/OU=CompanySectionName/CN=CommonNameOrHostname";
  sleep 86400
done
